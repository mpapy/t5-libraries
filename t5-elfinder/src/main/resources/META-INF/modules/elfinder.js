define([ "jquery", "elf/elf", "elf/i18n", "tjq/vendor/ui/jquery-ui.custom"], function($) {
	var init;
	getUrlParam = function(paramName) {
	    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
	    var match = window.location.search.match(reParam) ;
	    
	    return (match && match.length > 1) ? match[1] : '' ;
	};
	
	init = function(elemId,callbackUrl) {
		var funcNum = getUrlParam('CKEditorFuncNum');
		var elf = $('#' + elemId);
		elf.elfinder({
						url : callbackUrl,
						getFileCallback : function(file) {
											window.opener.CKEDITOR.tools.callFunction(funcNum, file.url);
											window.close();
										},
						resizable: false
					}).elfinder('instance');
	};
	
	return init;
});