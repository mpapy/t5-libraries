/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import java.io.File;

import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;

public class PasteCommand extends AbstractCommand {

	/**
	 * Constructor
	 * @param request the request to process
	 */
	public PasteCommand(Request request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		File dirSrc;
		try {
			dirSrc = getVolume().getFileFromHash(getParameter("src"));
		} catch (VolumeIOException e1) {
			throw new ElFinderException(e1.getMessage(),e1);
		}

		if (dirSrc != null) {

			File dirDst;
			try {
				dirDst = getVolume().getFileFromHash(getParameter("dst"));
			} catch (VolumeIOException e1) {
				throw new ElFinderException(e1.getMessage(),e1);
			}
			if (dirDst != null) {

				String[] targets = getParameters("targets[]");
				if (targets == null || targets.length == 0) {
					throw new ElFinderException("Invalid parameters");
				}

				boolean cut = "1".equals(getParameter("cut"));

				for (String targetHash : targets) {
					File fileTarget;
					try {
						fileTarget = getVolume().getFileFromHash(targetHash, dirSrc);
					} catch (VolumeIOException e1) {
						throw new ElFinderException(e1.getMessage(),e1);
					}
					if (fileTarget == null) {
						try {
							_content(getWorkingFile(), true);
						} catch (VolumeIOException e) {
							throw new ElFinderException(e.getMessage(), e);
						}
						throw new ElFinderException("File not found");
					}

					if (dirSrc.getAbsolutePath().equals(dirDst.getAbsolutePath())) {
						try {
							_content(getWorkingFile(), true);
						} catch (VolumeIOException e) {
							throw new ElFinderException(e.getMessage(), e);
						}
						throw new ElFinderException("Unable to copy into itself");
					}

					File futureFile;
					try {
						futureFile = getVolume().getNewFile(basename(fileTarget), dirDst);
					} catch (VolumeIOException e1) {
						throw new ElFinderException(e1.getMessage(),e1);
					}

					if (cut) {
						try {
							getVolume().rename(fileTarget, futureFile);
							_content(getWorkingFile(), true);
						} catch (VolumeIOException e) {
							throw new ElFinderException("Unable to move files");
						}
					} else {
						// copying file...
						try {
							getVolume().copy(fileTarget, futureFile);
							_content(getWorkingFile(), true);
						} catch (VolumeIOException e) {
							throw new ElFinderException("Unable to copy files");
						}
					}
				}
			}
		}
		try {
			_content(getWorkingFile(), true);
		} catch (VolumeIOException e) {
			throw new ElFinderException(e.getMessage(), e);
		}
		return getJSON();
	}
}
