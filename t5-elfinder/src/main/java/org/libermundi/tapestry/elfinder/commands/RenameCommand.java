package org.libermundi.tapestry.elfinder.commands;

import java.io.File;

import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;

public class RenameCommand extends AbstractCommand {

	/**
	 * Constructor
	 * @param request the request to process
	 */
	public RenameCommand(Request request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		File targetFile, futureFile;
		try {
			targetFile = getVolume().getFileFromHash(getParameter("target"), getWorkingFile());
			futureFile = getVolume().getNewFile(getParameter("name"), getWorkingFile());
			getVolume().rename(targetFile, futureFile);
			putResponse("select", getVolume().hash(targetFile));
			_content(getWorkingFile(), true);
		} catch (VolumeIOException e) {
			throw new ElFinderException(e.getMessage(), e);
		}

		return getJSON();
	}

}
