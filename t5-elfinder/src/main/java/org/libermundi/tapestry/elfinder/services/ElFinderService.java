/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.services;

import java.util.List;

import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;

/**
 * @author Martin Papy
 *
 */
public interface ElFinderService {
	
	/**
	 * Simply execute the command based on the {@link Request} passed on.
	 * @param request the {@link Request} to process
	 * @return the result of the command to execute
	 * @throws ElFinderException if anything goes unexpected
	 */
	public Object processRequest(Request request) throws ElFinderException;
	
	/**
	 * @param parameterName is the name of the Form parameter matching the upload request
	 * @return the list of uploaded files
	 */
	List<UploadedFile> getUploadedFiles(String parameterName);
}
