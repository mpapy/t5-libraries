/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import java.io.File;
import java.util.List;

import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;

/**
 * 
 * @author Martin Papy
 *
 */
public class UploadCommand extends AbstractCommand {
	
	/**
	 * Constructor
	 * @param request the request to process
	 */
	public UploadCommand(Request request){
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		JSONArray added = new JSONArray();
		List<UploadedFile> uploadedFiles = getElFinderService().getUploadedFiles("upload[]");

		for (UploadedFile uplFileEntry : uploadedFiles) {

			checkUploadSizes(uplFileEntry.getSize());
			
			File newFile;
			try {
				newFile = getVolume().save(uplFileEntry, getWorkingFile());
				added.put(_infos(newFile));
			} catch (VolumeIOException e) {
				throw new ElFinderException(e.getMessage(),e);
			}
  
		}

		putResponse("added", added);
		return getJSON();
	}
	
	protected void checkUploadSizes(long uploadSizeOctets) throws ElFinderException {
		// Check current total size of the Volume
		long totalSizeOctets = getVolume().getTotalSize();
		
		// Check if the file size is bellow the max upload size
		if (uploadSizeOctets > getVolume().getMaxUploadSize()) {
			throw new ElFinderException("File size exceed the maximum allowed value (" + getVolume().getMaxUploadSize()/1024 + " KB)");
		}
		
		// Check if the total size of the Volume (after the upload) would still be bellow Max Size limit
		if (getVolume().getMaxSize() > -1 && (totalSizeOctets + uploadSizeOctets) > getVolume().getMaxSize()) {
			throw new ElFinderException("The size of the Volume (after upload) would exceed the maximum allowed value (" + getVolume().getMaxSize()/1024 + " KB)");
		}
	}
	
}
