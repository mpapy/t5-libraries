/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.services;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.apache.tapestry5.upload.services.UploadedFile;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;

/**
 * Represent a "Volume" for ElFinder. A Volume is like a Drive mounted in the File Browser
 * One ElFinder can provide access to several {@link Volume}s
 * 
 * The {@link Volume} is responsible for all I/O operations and Security checkings
 * 
 * @author Martin Papy
 *
 */
public interface Volume {
	/**
	 * @return the Root Directory matching this specific {@link Volume}
	 */
	File getBasePath(); 
	
	/**
	 * 
	 * @return the corresponding URL to the Root Directory
	 */
	String getBaseUrl();

	/**
	 * Return the right URL for the Client to access to a given file
	 * @param file the file to seek the URL for
	 * @return the URL in the client format
	 * @throws VolumeIOException if the {@link File} does not exist
	 */
	String getClientUrl(File file) throws VolumeIOException;
	
	/**
	 * ElFinder identifies files using a specific hash ( volumeId + _  + hash ). This method allows
	 * to retried a File URL using the hash as a parameter
	 * @param hash the hash of the file to retried
	 * @return the URL in the client format
	 * @throws VolumeIOException if the {@link File} does not exist
	 */
	String getClientUrl(String hash) throws VolumeIOException;
	
	/**
	 * Look for a File or Directory based on the provided hash.
	 * The search is performed in the whole volume. 
	 * @param targetHash the hash to analyze
	 * @return the corresponding {@link File} or <code>null</code>
	 * @throws VolumeIOException if the {@link File} does not exist
	 */
	File getFileFromHash(String targetHash) throws VolumeIOException;
	
	/**
	 * Look for a File or Directory based on the provided hash.
	 * The search is performed within the specified Directory or its subfolders. 
	 * @param targetHash the hash to analyze
	 * @param folder the folder to look into
	 * @return the corresponding {@link File} or <code>null</code>
	 * @throws VolumeIOException if the {@link File} does not exist
	 */
	File getFileFromHash(String targetHash, File folder) throws VolumeIOException;	
	
	/**
	 * Generate a Hash compatible with ElFinder specifications ( volumeId + _  + hash )
	 * @param path the File to calculate the hash for
	 * @return the generated hash
	 */
	String hash(File path);
	
	/**
	 * Return the ID of the Volume. Must be UNIQUE.
	 * @return a String representing the ID
	 */
	String getId();
	
	/**
	 * Return true if the path represents either a Directory or is a File for which the
	 * extension is allowed in this {@link Volume}
	 * @param path The file to check the extension
	 * @return true if the extension is allowed.
	 */
	boolean isAllowed(File path);

	/**
	 * Return true if the path represents either a Directory or is a file for which the
	 * extension is allowed in this {@link Volume} and matches the {@link FileType}
	 * @param path The file to check the extension
	 * @param type restrict to a specific {@link FileType}
	 * @return true if the extension is allowed.
	 */
	boolean isAllowed(File path, FileType type);
	
	/**
	 * Return true if the path represents either a Directory or is a File for which the
	 * extension is allowed in this {@link Volume}
	 * @param filename the filename to analyze
	 * @return true if the extension is allowed.
	 */
	boolean isAllowed(String filename);

	/**
	 * Return true if the filename extension is allowed in this
	 * {@link Volume} and matches the {@link FileType}
	 * @param filename the filename to analyze
	 * @param type restrict to a specific {@link FileType}
	 * @return true if the extension is allowed.
	 */
	boolean isAllowed(String filename, FileType type);
	
	/**
	 * Add a list of extension to be allowed on this Volume. Extension are case insensitive
	 * @param type {@link FileType} to add allowed extension to
	 * @param extensions comma separated list of extensions 
	 */
	void allowExtension(FileType type, String extensions);
	
	/**
	 * Add a list of extension to be allowed on this Volume. Extension are case insensitive
	 * @param type {@link FileType} to add allowed extension to
	 * @param extensions String array of extensions 
	 */
	void allowExtension(FileType type, String... extensions);
	
	/**
	 * Create a new empty file in the specified directory
	 * @param fileName the name of the File to create. Have to be unique.
	 * @param existingDir the Directory to create the file in
	 * @return the newly created {@link File}
	 * @throws VolumeIOException if the Directory does not exist, if the File already exists or the filename is not valid
	 */
	File getNewFile(String fileName, File existingDir) throws VolumeIOException;
	
	/**
	 * Create a new empty file in the specified directory
	 * @param fileName the name of the File to create
	 * @param existingDir the Directory to create the file in
	 * @param makeUnique is set to true, a new name will be generated if a similar file name already exists.
	 * @return the newly created {@link File}
	 * @throws VolumeIOException if the Directory does not exist, if the File already exists (and makeUnique is false) or the filename is not valid
	 */
	File getNewFile(String fileName, File existingDir, boolean makeUnique) throws VolumeIOException;

	/**
	 * Check if the filename provided complies with the naming rule of the {@link Volume}
	 * @param fileName the filename to check
	 * @return <code>true</code> when the name is compliant
	 */
	boolean isValidFilename(String fileName);

	/**
	 * @return the alias of this Volume
	 */
	String getAlias();

	/**
	 * Checks is the file is "." or ".."
	 * @param file the file to check
	 * @return true if it is
	 */
	boolean isSpecialDir(File file);

	/**
	 * Checks if the requested {@link VolumeOperation} is allowed for that {@link File}
	 * @param file the {@link File} to check
	 * @param operation the {@link VolumeOperation} to check
	 * @return <code>true</code> if it is.
	 */
	boolean isOperationAllowed(File file, VolumeOperation operation);

	/**
	 * Returns the relative URL to this file within this {@link Volume} ( ie : relative to the root of the Volume )
	 * @param file the file to get IRL for
	 * @return the relative URL (in form of a String)
	 * @throws VolumeIOException if the {@link File} does not exist
	 */
	String getRelativeUrlPath(File file) throws VolumeIOException;

	/**
	 * @return the path separator used by this {@link Volume}
	 */
	String getPathSeparator();

	/**
	 * Check is this {@link File} already as a corresponding Thumbnail
	 * @param f the {@link File} to check
	 * @return true if it is
	 */
	boolean hasThumbnail(File f);

	/**
	 * Get the Thumbnail URL for a File
	 * @param f the file
	 * @return the String representation of the URL
	 * @throws VolumeIOException if the {@link File} does not exist 
	 */
	String getThumbnailUrl(File f) throws VolumeIOException;

	/**
	 * Get the maximal size of the file use can upload for this Volume (in bytes)
	 * @return the max upload size in bytes
	 */
	long getMaxUploadSize();
	
	/**
	 * @return the maximum size in bytes of the Volume
	 * By default this should be unlimited ( -1 )
	 */
	long getMaxSize();
	
	/**
	 * Creates a new Folder in the {@link Volume}
	 * @param newDir the new Folder to create
	 * @throws VolumeIOException if the operation is not allowed 
	 */
	void createFolder(File newDir) throws VolumeIOException;

	/**
	 * Saves a new {@link File} on the {@link Volume}
	 * @param newFile the file to create
	 * @param os the source of the content of the file
	 * @throws VolumeIOException if the operation is not allowed or the file cannot be create/written for some reason
	 */
	void createFile(File newFile, ByteArrayOutputStream os) throws VolumeIOException;

	/**
	 * Renames an existing file
	 * @param fileTarget orginal file
	 * @param futureFile new file ( new name )
	 * @throws VolumeIOException if the operation is not allowed or either the source / destination does not belong to the {@link Volume}
	 */
	void rename(File fileTarget, File futureFile) throws VolumeIOException;

	/**
	 * Performs a copy of an existing file
	 * @param fileTarget original file
	 * @param futureFile copy
	 * @throws VolumeIOException if the operation is not allowed or either the source / destination does not belong to the {@link Volume}
	 */
	void copy(File fileTarget, File futureFile) throws VolumeIOException;

	/**
	 * Deletes an existing file
	 * @param path file to delete
	 * @throws VolumeIOException if the operation is not allowed or the file does not exist
	 */
	void delete(File path) throws VolumeIOException;

	/**
	 * @return the total size of the {@link Volume} in bytes
	 */
	long getTotalSize();

	/**
	 * Saves an {@link UploadedFile} in the {@link Volume}
	 * @param file the {@link UploadedFile}
	 * @param workingDirectory where to save it
	 * @param type the expected {@link FileType} of the uploaded file
	 * @return the saved {@link File}
	 * @throws VolumeIOException if the operation is not allowed or the location does not exist
	 */
	File save(UploadedFile file, File workingDirectory, FileType type) throws VolumeIOException;

	/**
	 * Saves an {@link UploadedFile} in the {@link Volume}
	 * @param file the {@link UploadedFile}
	 * @param workingDirectory where to save it
	 * @return the saved {@link File}
	 * @throws VolumeIOException if the operation is not allowed or the location does not exist
	 */
	File save(UploadedFile file, File workingDirectory) throws VolumeIOException;

}
