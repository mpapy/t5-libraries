package org.libermundi.tapestry.elfinder.commands;

import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;

public class OpenCommand extends AbstractCommand {

	/**
	 * Constructor
	 * @param request the request to process
	 */
	public OpenCommand(Request request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		try {
			_contentCommand();
		} catch (VolumeIOException e) {
			throw new ElFinderException(e.getMessage(), e);
		}
		return getJSON(); 
	}
}
