/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;

/**
 * @author Martin Papy
 *
 */
public class CommandFactory {
	private CommandFactory(){
		
	}
	
	/**
	 * Return the proper {@link ElFinderCommand} based on the analysis of the {@link Request}
	 * @param request the {@link Request} to process
	 * @return the corresponding {@link ElFinderCommand}
	 * @throws ElFinderException if the {@link Request} is not properly formated ( ie : command does not exist )
	 */
	public static ElFinderCommand getCommand(Request request) throws ElFinderException {
		String command = request.getParameter("cmd").toLowerCase();
		if(command.equals("open")) {
			return new OpenCommand(request);
		}
		if(command.equals("tree")) {
			return new TreeCommand(request);
		}
		if(command.equals("mkdir")) {
			return new MkdirCommand(request);
		}
		if(command.equals("mkfile")) {
			return new MkfileCommand(request);
		}
		if(command.equals("rm")) {
			return new RmCommand(request);
		}
		if(command.equals("rename")) {
			return new RenameCommand(request);
		}
		if(command.equals("upload")) {
			return new UploadCommand(request);
		}
		if(command.equals("paste")) {
			return new PasteCommand(request);
		}
		if(command.equals("parents")) {
			return new ParentsCommand(request);
		}
		if(command.equals("dim")) {
			return new DimCommand(request);
		}
		throw new ElFinderException("Error Unknown Command");
	}
	
	

}
