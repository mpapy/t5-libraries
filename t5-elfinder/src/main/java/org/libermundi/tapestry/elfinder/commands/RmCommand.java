/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import java.io.File;

import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;


/**
 * 
 * @author Martin Papy
 *
 */
public class RmCommand extends AbstractCommand {

	/**
	 * Constructor
	 * @param request the request to process
	 */
	public RmCommand(Request request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		JSONArray removed = new JSONArray();
		String[] targets = getParameters("targets[]");
		if (targets == null || targets.length == 0 ) {
			throw new ElFinderException("Invalid parameters");
		}

		for (String targetHash : targets) {
			File fileTarget;
			try {
				fileTarget = getVolume().getFileFromHash(targetHash, getWorkingFile());
			} catch (VolumeIOException e) {
				throw new ElFinderException(e.getMessage(),e);
			}
			
			if (fileTarget == null) {
				throw new ElFinderException("File not found");
			}
			_remove(fileTarget);
			removed.put(targetHash);
		}
		putResponse("removed", removed);
		return getJSON(); 
	}

	/**
	 * Remove file or folder (recursively)
	 **/
	protected void _remove(File path) throws ElFinderException {
		if (path == null || !path.exists()) {
			throw new ElFinderException("Invalid parameters");
		}
		try {
			getVolume().delete(path);
		} catch (VolumeIOException e) {
			throw new ElFinderException(e.getMessage(),e);
		}
	}

}
