/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.services.impl;

import java.io.File;

import org.libermundi.tapestry.elfinder.exception.VolumeIOException;
import org.libermundi.tapestry.elfinder.services.Volume;
import org.libermundi.tapestry.elfinder.services.VolumeOperation;

/**
 * Very simple and not really safe implementation of a {@link Volume}
 * All {@link VolumeOperation}s are allowed by default
 * MaxUploadSize is set to 8Mb
 * 
 * @author Martin Papy
 *
 */
public class SimpleVolume extends AbstractVolume {

	public SimpleVolume(String volumeId, File basePath, String baseUrl,
			long maxSize, String volumeAlias) {
		super(volumeId, basePath, baseUrl, maxSize, volumeAlias);
	}

	/**
	 * Constructor for the SimpleVolume
	 * @param basePath the physical root of the Volume
	 * @param baseUrl the URL corresponding to the root of the Volume
	 * @param maxSize the maximum size ( in bytes ) for the Volume itself
	 */
	public SimpleVolume(File basePath, String baseUrl, long maxSize) {
		super(basePath, baseUrl, maxSize);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.Volume#isOperationAllowed(java.io.File, org.libermundi.tapestry.elfinder.services.VolumeOperation)
	 */
	@Override
	public boolean isOperationAllowed(File file, VolumeOperation operation) {
		return Boolean.TRUE;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.Volume#getThumbnailUrl(java.io.File)
	 */
	@Override
	public String getThumbnailUrl(File f) throws VolumeIOException {
		// SimpleVolume generate Thumbnails on Event base
		return null;
	}

}
