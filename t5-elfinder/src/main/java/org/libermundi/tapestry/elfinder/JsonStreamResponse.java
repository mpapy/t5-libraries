package org.libermundi.tapestry.elfinder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.services.Response;

public class JsonStreamResponse implements StreamResponse {

      private String text;

      public JsonStreamResponse(String text) {
         this.text = text;
      }

      @Override
      public String getContentType() {
         return "text/html";
      }

      @Override
      public InputStream getStream() throws IOException {
         return new ByteArrayInputStream(text.toString().getBytes());
      }

      @Override
      public void prepareResponse(Response response) {
    	  //Nothing to do here
      }

}