/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;
import org.libermundi.tapestry.elfinder.services.ElFinderService;
import org.libermundi.tapestry.elfinder.services.VolumeSource;

/**
 * @author Martin Papy
 *
 */
public interface ElFinderCommand {
	/**
	 * Execute the requested command
	 * @return the result of the command
	 * @throws ElFinderException if anything gies unexpected
	 */
	public Object execute() throws ElFinderException;
	
	/**
	 * Getter
	 * @return the related {@link ElFinderService}
	 */
	public ElFinderService getElFinderService();
	
	/**
	 * Setter
	 * @param service sets the {@link ElFinderService}
	 */
	public void setElFinderService(ElFinderService service);

	/**
	 * Attach a {@link VolumeSource} to the Command
	 * @param volumeSource Defines the {@link VolumeSource} to operated on
	 */
	void setVolumeSource(VolumeSource volumeSource);

	/**
	 * Initialize the Volume
	 * @throws VolumeIOException if there is a problem 
	 */
	void initVolumeAndCwd() throws VolumeIOException;
}
