/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.pages;

import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.corelib.components.Any;
import org.apache.tapestry5.internal.EmptyEventContext;
import org.apache.tapestry5.internal.services.ArrayEventContext;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.services.TypeCoercer;
import org.apache.tapestry5.services.ComponentEventLinkEncoder;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

/**
 * @author Martin Papy
 *
 */
@Import(stylesheet={
			"${elfinder.library}/css/elfinder.min.css",
			"${elfinder.library}/css/theme.css"
		}
)
public class ElFinder {
	@Component
	private Any _elFinder;
	
	@Inject
	private JavaScriptSupport _jsSupport;
	
	@Inject
	private ComponentEventLinkEncoder _componentEventLinkEncoder;
	
    @Inject
    private TypeCoercer _typeCoercer;
    
	@AfterRender
	public void afterRender(){
        ComponentEventRequestParameters parameters = new ComponentEventRequestParameters("elfinder/ElFinderConnector", "elfinder/ElFinderConnector", "",
        		ElFinderConnector.ELFINDER_EVENT, new ArrayEventContext(_typeCoercer), new EmptyEventContext());
        
		_jsSupport.require("elfinder").with(_elFinder.getClientId(),_componentEventLinkEncoder.createComponentEventLink(parameters, false).toURI());
	}
	

}
