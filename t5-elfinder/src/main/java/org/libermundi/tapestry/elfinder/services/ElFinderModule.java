/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.services;

import java.util.List;

import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.ioc.ScopeConstants;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Local;
import org.apache.tapestry5.services.LibraryMapping;
import org.apache.tapestry5.services.javascript.JavaScriptModuleConfiguration;
import org.apache.tapestry5.services.javascript.ModuleManager;
import org.apache.tapestry5.upload.services.MultipartDecoder;
import org.libermundi.tapestry.ckeditor.CKEditorContants;
import org.libermundi.tapestry.elfinder.ElFinderConstants;
import org.libermundi.tapestry.elfinder.services.impl.ElFinderServiceImpl;
import org.libermundi.tapestry.elfinder.services.impl.MultipleUploadDecoderImpl;
import org.libermundi.tapestry.elfinder.services.impl.VolumeSourceImpl;


/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
public class ElFinderModule {
	public static void contributeComponentClassResolver(final Configuration< LibraryMapping > configuration) {
        configuration.add(new LibraryMapping(ElFinderConstants.TAPESTRY_MAPPING, "org.libermundi.tapestry.elfinder"));
	}
	
	public static void bind(ServiceBinder binder) {
		binder.bind(MultipleUploadDecoder.class, MultipleUploadDecoderImpl.class).scope(ScopeConstants.PERTHREAD);
		binder.bind(ElFinderService.class, ElFinderServiceImpl.class);
	}

	public static VolumeSource buildVolumeSource(final List<Volume> volumes) {
		return new VolumeSourceImpl(volumes);
	}
	
	 public static void contributeApplicationDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add(CKEditorContants.SYMBOL_FILEBROWSER_URL, "/elfinder/elfinder");
	}

	public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add(ElFinderConstants.ASSETS, "META-INF/assets");
        
        configuration.add(ElFinderConstants.ELFINDER_LIBRARY, "${elfinder.assets}/elfinder_2_1");

    }
	
    public static void contributeServiceOverride(@Local MultipleUploadDecoder multipartDecoder,
            @SuppressWarnings("rawtypes")
    		MappedConfiguration<Class, Object> overrides) {
         overrides.add(MultipartDecoder.class, multipartDecoder);
    }
    
	@Contribute(ModuleManager.class)
	public static void setupComponentsShims(
			MappedConfiguration<String, Object> configuration,
			@Inject @Path("${elfinder.library}/js/elfinder.min.js") Resource elFinder,
			@Inject @Path("${elfinder.library}/js/i18n/elfinder.js") Resource elFinderi18n) {

		configuration.add("elf/elf",
				new JavaScriptModuleConfiguration(elFinder));
		configuration.add("elf/i18n",
				new JavaScriptModuleConfiguration(elFinderi18n).dependsOn("elf/elf"));
	}  
}
