/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.services.FileType;


/**
 * 
 * @author Martin Papy
 *
 */
public class DimCommand extends AbstractCommand {

	/**
	 * Constructor
	 * @param request the request to process
	 */
	public DimCommand(Request request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		if(getWorkingFile().isFile() && getVolume().isAllowed(getWorkingFile(), FileType.IMAGE)) {
			BufferedImage image;
			try {
				image = ImageIO.read(getWorkingFile());
			} catch (IOException e) {
				throw new ElFinderException("There was an error while reading the File", e);
			}
			putResponse("dim",image.getWidth() + "x" + image.getHeight());
			return getJSON(); 
		}
		throw new ElFinderException("Target is not an Image");
	}


}
