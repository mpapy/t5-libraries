/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.services.impl;

import java.util.List;

import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.upload.services.MultipartDecoder;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.libermundi.tapestry.elfinder.commands.CommandFactory;
import org.libermundi.tapestry.elfinder.commands.ElFinderCommand;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;
import org.libermundi.tapestry.elfinder.services.ElFinderService;
import org.libermundi.tapestry.elfinder.services.MultipleUploadDecoder;
import org.libermundi.tapestry.elfinder.services.VolumeSource;

/**
 * @author Martin Papy
 *
 */
public class ElFinderServiceImpl implements ElFinderService {
	
	private MultipartDecoder _multipartDecoder;
	
	private VolumeSource _volumeSource;
	
	public ElFinderServiceImpl(
			//MultipartDecoder multipartDecoder,
			MultipartDecoder multipartDecoder,
			VolumeSource volumeSource) {
		_multipartDecoder = multipartDecoder;
		_volumeSource = volumeSource;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.security.tapestry.services.elfinder.ElFinderService#processRequest(org.apache.tapestry5.services.Request)
	 */
	@Override
	public Object processRequest(Request request) throws ElFinderException {
		ElFinderCommand command = CommandFactory.getCommand(request);
			command.setElFinderService(this);
			command.setVolumeSource(_volumeSource);
			try {
				command.initVolumeAndCwd();
			} catch (VolumeIOException e) {
				throw new ElFinderException(e.getMessage(), e);
			}
		return command.execute();
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.ElFinderService#getUploadedFiles()
	 */
	@Override
	public List<UploadedFile> getUploadedFiles(String parameterName) {
		return ((MultipleUploadDecoder)_multipartDecoder).getFilesUpload(parameterName);
	}

	
}