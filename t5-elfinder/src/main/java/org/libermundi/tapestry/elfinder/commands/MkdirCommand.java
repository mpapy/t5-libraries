/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.commands;

import java.io.File;

import org.apache.tapestry5.services.Request;
import org.libermundi.tapestry.elfinder.exception.ElFinderException;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;
/**
 * 
 * @author Martin Papy
 *
 */
public class MkdirCommand extends AbstractCommand{

	/**
	 * Constructor
	 * @param request the request to process
	 */
	public MkdirCommand(Request request) {
		super(request);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.commands.ElFinderCommand#execute()
	 */
	@Override
	public Object execute() throws ElFinderException {
		File newDir;

		try {
			newDir = getVolume().getNewFile(getParameter("name"), getWorkingFile());
			getVolume().createFolder(newDir);
			putResponse("added", _infos(newDir));
		} catch (VolumeIOException e) {
			throw new ElFinderException(e.getMessage(), e);
		}

		
		return getJSON();
	}


}
