/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.elfinder.services;

import java.util.List;

import org.libermundi.tapestry.elfinder.exception.VolumeIOException;

/**
 * Simple Service Interface designed to return the list of available {@link Volume}s for ElFinder 
 * @author Martin Papy
 *
 */
public interface VolumeSource {
	
	/**
	 * Add a new {@link Volume} to this source
	 * @param volume the Volume to add
	 */
	public void add(Volume volume);
	
	/**
	 * Add a new {@link Volume} to this source
	 * @param volume the Volume to add
	 * @param isdefault true will make this newly added Volume the default one.
	 */
	public void add(Volume volume, boolean isdefault);
	
	/**
	 * Set the provided {@link Volume} as Default. If the {@link Volume} was not added already, then
	 * add the {@link Volume}
	 * @param volume the Volume to become the default one.
	 */
	public void setDefault(Volume volume);
	
	/**
	 * Gets the {@link List} of {@link Volume} in the source
	 * @return the list of {@link Volume}
	 */
	public List<Volume> getVolumes();

	/**
	 * Returns a {@link Volume} based on an hash description
	 * @param targetHash the hash describing the {@link Volume}
	 * @return the corresponding {@link Volume}
	 * @throws VolumeIOException if the {@link Volume} does not exist
	 */
	public Volume getVolumeFromHash(String targetHash) throws VolumeIOException;

	/**
	 * Returns the default {@link Volume} if defined, the first {@link Volume} added if not
	 * @return the default {@link Volume}
	 */
	public Volume getDefaultVolume();

}
