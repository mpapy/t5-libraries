/*
 * Copyright (c) 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(["jquery", "t5/core/events", "t5/core/forms"], function($, events) {
	var formEventHandlers, init;
	
	formEventHandlers = {}; // To keep track of the CKEditor instances
	
	init = function(textareaId, ckeditorInitJSON) {
		/*
		 * If the textarea with id=ckeditorId cannot be found (probably because if
		 * tapestry zone update), than destroy it's corresponding ckeditor instance.
		 */
		for (ckeditorId in CKEDITOR.instances)
			if ($(ckeditorId) == undefined)
				// destroy the ckeditor instance without updating the textarea
				CKEDITOR.instances[ckeditorId].destroy(true);
	
		// init CKEditor for the given textarea
		CKEDITOR.replace(textareaId, ckeditorInitJSON);
		
		var updateTextArea = function() {
			/*
			 * if the ckeditor instance with id=textareaId cannot be found, than it
			 * has been destroyed so remove the corresponding eventHandler listening
			 * on FORM_PREPARE_FOR_SUBMIT_EVENT,
			 * 
			 * else update the textarea with the ckeditor contentnts before the form
			 * is submitted so that the corresponding server side property is
			 * updated.
			 */
			var ckeditorInstance = CKEDITOR.instances[textareaId];
			if (ckeditorInstance == undefined)
				document.stopObserving(events.form.prepareForSubmit,
						formEventHandlers[textareaId]);
			else
				ckeditorInstance.updateElement(); // update the textarea
		};
		
    	formEventHandlers[textareaId] = updateTextArea;
    	$(document).bind(events.form.prepareForSubmit, updateTextArea);	    	
	};

	return init;

});