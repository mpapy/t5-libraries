/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.ckeditor.services;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.services.LibraryMapping;
import org.libermundi.tapestry.ckeditor.CKEditorContants;

public class CKEditorModule {
	
	public static void contributeComponentClassResolver(final Configuration< LibraryMapping > configuration) {
        configuration.add(new LibraryMapping(CKEditorContants.TAPESTRY_MAPPING, "org.libermundi.tapestry.ckeditor"));
	}	
	
	public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add(CKEditorContants.ASSETS, "META-INF/assets");
        
        configuration.add(CKEditorContants.SYMBOL_CKEDITOR_VERSION, "latest");

        configuration.add(CKEditorContants.CKEDITOR36_LIBRARY, "${ckeditor.assets}/ckeditor_3_6_6_1");
        configuration.add(CKEditorContants.CKEDITOR41_LIBRARY, "${ckeditor.assets}/ckeditor_4_1_3");
        configuration.add(CKEditorContants.CKEDITOR42_LIBRARY, "${ckeditor.assets}/ckeditor_4_2_3");
        configuration.add(CKEditorContants.CKEDITOR43_LIBRARY, "${ckeditor.assets}/ckeditor_4_3_5");
        configuration.add(CKEditorContants.CKEDITOR44_LIBRARY, "${ckeditor.assets}/ckeditor_4_4_7");

        configuration.add(CKEditorContants.SYMBOL_FILEBROWSER_URL, "");
        configuration.add(CKEditorContants.SYMBOL_CUSTOMCONFIG, "${ckeditor.assets}/js/config.js");

     }
	
}
