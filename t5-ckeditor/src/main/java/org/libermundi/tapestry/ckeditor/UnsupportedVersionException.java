/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 * * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.tapestry.ckeditor;

/**
 * Created by Martin Papy on 3/7/14.
 */
public class UnsupportedVersionException extends RuntimeException {

    public UnsupportedVersionException(String version) {
        super("This version of CKEditor ( " + version + " ) is not supported. Allowed value for the version parameter : 4.3, 4.2, 4.1, 3.6 or latest");
    }
}
