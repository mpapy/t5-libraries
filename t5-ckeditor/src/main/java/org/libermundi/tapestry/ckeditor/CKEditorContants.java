/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.ckeditor;

/**
 * @author Martin Papy
 *
 */
public class CKEditorContants {
	private CKEditorContants(){}

	public static final String TAPESTRY_MAPPING = "ckeditor";

	public static final String ASSETS = "ckeditor.assets";
	public static final String CKEDITOR36_LIBRARY = "ckeditor.library_v36";
    public static final String CKEDITOR41_LIBRARY = "ckeditor.library_v41";
    public static final String CKEDITOR42_LIBRARY = "ckeditor.library_v42";
    public static final String CKEDITOR43_LIBRARY = "ckeditor.library_v43";
    public static final String CKEDITOR44_LIBRARY = "ckeditor.library_v44";

    public static final String SYMBOL_CKEDITOR_VERSION = "ckeditor.symbols.version";
	public static final String SYMBOL_FILEBROWSER_URL = "ckeditor.symbols.filebrowser_url";
	public static final String SYMBOL_CUSTOMCONFIG = "ckeditor.symbols.customconfig";

}