/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.ckeditor.mixins;

import java.util.Map;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.tapestry.ckeditor.CKEditorContants;
import org.libermundi.tapestry.ckeditor.UnsupportedVersionException;

import com.google.common.base.Strings;

/**
 * A mixin that replaces a {@link TextArea} component with a CKEditor instance. The initial value in
 * the ckeditor will be the textarea value.
 * 
 * @tapestrydoc
 */
public class CKEditor {
	/**
	 * The specific configurations to apply to this editor instance. Configurations set here will
	 * override global CKEditor settings.
	 * <p>
	 * See the <a href="http://docs.cksource.com/ckeditor_api/index.html">ckeditor documentation</a>
	 * for more details.
	 */
	@Parameter
	private Map<String, ?> _ckParameters;

    @Parameter(defaultPrefix = "literal")
    private String _ckVersion;

	@InjectContainer
	private TextArea _textArea;

	@Inject
	private JavaScriptSupport _javaScriptSupport;
	
	@Inject
	@Symbol(CKEditorContants.SYMBOL_FILEBROWSER_URL)
	private String _fileBrowser;
	
	@Inject
	@Path("${ckeditor.library_v36}/ckeditor.js")
	private Asset _ckeditor36;

    @Inject
    @Path("${ckeditor.library_v41}/ckeditor.js")
    private Asset _ckeditor41;

    @Inject
    @Path("${ckeditor.library_v42}/ckeditor.js")
    private Asset _ckeditor42;

    @Inject
    @Path("${ckeditor.library_v43}/ckeditor.js")
    private Asset _ckeditor43;

    @Inject
    @Path("${ckeditor.library_v44}/ckeditor.js")
    private Asset _ckeditor44;
	
	@Inject
	@Symbol(CKEditorContants.SYMBOL_CKEDITOR_VERSION)
	private String _defaultVersion;
	
	@Inject
	@Symbol(SymbolConstants.CONTEXT_PATH)
	private String _contextPath;
	
	@Inject
	@Path("${ckeditor.symbols.customconfig}")
	private Asset _customConfig;
	
	@Inject
	private Request _request;
	
	@AfterRender
	void afterRender(MarkupWriter writer) {
        String useVersion = _defaultVersion;
        if(!Strings.isNullOrEmpty(_ckVersion)){
            useVersion = _ckVersion;
        }

        if(useVersion.equals("latest")){
            _javaScriptSupport.importJavaScriptLibrary(_ckeditor44);
        } else if (useVersion.equals("4.4")) {
            _javaScriptSupport.importJavaScriptLibrary(_ckeditor43);
        } else if (useVersion.equals("4.3")) {
            _javaScriptSupport.importJavaScriptLibrary(_ckeditor43);
        } else if (useVersion.equals("4.2")) {
            _javaScriptSupport.importJavaScriptLibrary(_ckeditor42);
        } else if (useVersion.equals("4.1")) {
            _javaScriptSupport.importJavaScriptLibrary(_ckeditor41);
        } else if (useVersion.equals("3.6")) {
            _javaScriptSupport.importJavaScriptLibrary(_ckeditor36);
        } else {
            throw new UnsupportedVersionException(useVersion);
        }

		String id = _textArea.getClientId();

		JSONObject json = new JSONObject();
		
		if(_customConfig != null && _customConfig.getResource().exists()){
			json.put("customConfig", _customConfig.toClientURL());
		}
		
		if(!Strings.isNullOrEmpty(_fileBrowser)){
			json.put("filebrowserBrowseUrl",_contextPath + _fileBrowser);
		}
		
		if (_ckParameters != null)
			for (String paramName : _ckParameters.keySet())
				json.put(paramName, _ckParameters.get(paramName));
		
		_javaScriptSupport.require("ckeditor").with(id, json);
	}
}
