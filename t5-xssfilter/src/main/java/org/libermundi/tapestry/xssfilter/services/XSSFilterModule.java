/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.tapestry.xssfilter.services;

import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Local;
import org.apache.tapestry5.services.RequestFilter;
import org.apache.tapestry5.services.RequestHandler;

/**
 * 
 * @author Martin Papy
 *
 */
public class XSSFilterModule {
	public static void bind(ServiceBinder binder) {
        binder.bind(RequestFilter.class, XSSRequestFilterImpl.class).withId("XSSRequestFilter");
	}   

	/*
	 * XSS Filtering
	 */
	@Contribute(RequestHandler.class)
	public static void requestHandler(OrderedConfiguration<RequestFilter> configuration,
												@Local
	                                            RequestFilter xssFilter)	{
		configuration.add("XSSRequestFilter", xssFilter, "after:StaticFiles", "before:StoreIntoGlobals");
	}
}
