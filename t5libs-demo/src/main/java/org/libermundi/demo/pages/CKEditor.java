/*
 * Copyright (c) 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.demo.pages;

import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;

import com.google.common.base.Strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Martin Papy on 3/18/14.
 */
public class CKEditor {
    @Property
    @Inject
    @Symbol(SymbolConstants.TAPESTRY_VERSION)
    private String tapestryVersion;

    @Property
    private String versionSelected;

    @Property
    private String configurationSelected;

    @Property
    @Persist
    private String description;

    @Property
    @Persist
    private String ckVersion;

    @Property
    @Persist
    private Map<String, String> ckParameters;

    public void setupRender(){
        if(Strings.isNullOrEmpty(description)){
           description = "This is a demo !";
        }

        if(Strings.isNullOrEmpty(ckVersion)){
            ckVersion = "latest";
        }

        if(ckParameters == null) {
            ckParameters = new HashMap<String, String>();
            ckParameters.put("toolbar", "Basic");
        }
    }

    public void onSuccess(){
        ckVersion = versionSelected;
        if(!Strings.isNullOrEmpty(configurationSelected) && configurationSelected.equals("Advanced")){
            ckParameters.put("toolbar","NoUpload");
        } else {
            ckParameters.put("toolbar","Basic");
        }
    }
}
