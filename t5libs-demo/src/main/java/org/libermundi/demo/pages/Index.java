package org.libermundi.demo.pages;


import org.apache.tapestry5.EventContext;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.HttpError;
import org.slf4j.Logger;

/**
 * Start page of application t5libs-apps2.
 */
public class Index
{
	@Inject
	private Logger logger;

	@Property
	@Inject
	@Symbol(SymbolConstants.TAPESTRY_VERSION)
	private String tapestryVersion;

	// Handle call with an unwanted context
	Object onActivate(EventContext eventContext)
	{
		return eventContext.getCount() > 0 ?
				new HttpError(404, "Resource not found") :
				null;
	}

}
